Local Cluster Playbooks
=======================

These playbooks are for managing local infrastructure located in TV1 (the bunker).

Layout
--------

![Topology](bc.png)

There are 16 blades (HP BL460c) located in the chassis.  There is also fircha
(the massive 2u server) located...somewhere.  All of this hardware collectively
presents a massive amount of computing power.

### Common architecture

All machines are running an iptables based firewall
([ferm](http://ferm-foo-projects.org/)), as well as openssh.  Normal users are
created on each of the machines, with their SSH keys copied from Github.  Keys
are copied from github to the sovee user for remote access.  NTP is running
across the cluster to ensure clocks remain in sync.

### Router (bc01)

This machine is the core of the cluster.  It provides Internet access to the
remainder of the blades in the chassis, as well as preventing unauthorized
access to the remaining blades.

#### Routing

bc01 has a fixed local IP of 10.15.199.201, which is NAT'd from an external IP
of 104.128.174.231 (office.sovee.com).  All outbound traffic from the remainder
of the BladeCenter is routed through bc01 and MASQUERADE'd as it leaves the
external interface.  None of the compute nodes have IP's on any public networks.

#### OpenVPN server (http://www.openvpn.com)

There are currently 2 VPN's configured:
1. The external VPN, which provides access to the cluster from the outside world (mainly developers accessing things remotely.
1. An internal VPN, mainly to allow fircha to participate in the cluster.  Without this, fircha would have no way of communicating with the remainder of the blades, since they are firewalled off from the rest of the world.

#### NTP server (http://www.openntpd.org)

Synchronizes time with some Internet based atomic clocks.  All other blades keep their clocks in sync with bc01.

#### PXE server

This is probably the most convenient role the router plays.  Rather than insert virtual media into blades when they need to be provisioned (reinstalled), they can be configured to boot via the network, and bc01 will bootstrap an OS automatically, with no intervention required.  Once complete, ansible can take over to install the actual software/users/configuration.

#### DHCP server

In concert with PXE booting, bc01 provides DHCP services for the blades.  This allows each blade to maintain a fixed IP address, without the need to manually configure the blade.  This is done by mapping IP's to MAC addresses on the NIC's, which are in turn controlled by the VirtualConnect profiles.  What this means is that, for a given slot in the chassis, any blade inserted there will ALWAYS have the same IP address, even if they physical hardware is swapped out.

#### DNS server

Handles queries for both internal hosts and web traffic.  Responds only on internal interfaces, so should not interfere with existing DNS.

Development
-----------
A Vagrantfile is available for developing/testing the various playbooks.  *A word of warning: the cluster requires quite a bit of RAM to spin up (at present, 5.4G), so consider this before attempting to create it locally.*  The dev cluster can be generated by simply executing:
```
# vagrant up
```
The cluster will take a while to build on the first run.  Once it has completed, the playbooks can be run against it.  The local development environment is described in the ansible inventory file `hosts.dev`.  To run all playbooks against the entire development cluster, simply run:
```
# ansible-playbook -i hosts.dev site.yml
```

This will provision the entire cluster according to the various roles defined.

### Rebuilding the development cluster
If at any time the development cluster needs to be rebuilt, ssh will complain about fingerprint discrepancies and ansible will refuse to run.  To correct this issue, all the machines will need to be removed from the `~/.ssh/known_hosts` file.  A simple one-liner to accomplish this:
```
# for h in {router,controller,compute1}; do ssh-keygen -R $h; done
```
