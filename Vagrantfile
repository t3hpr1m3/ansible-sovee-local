# -*- mode: ruby -*-
# vi: set ft=ruby :

%w(vagrant-hostmanager).each do |plugin|
  system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin
end

ROUTER_RAM = (ENV['ROUTER_RAM'] || 768).to_i
CONTROLLER_RAM = (ENV['CONTROLLER_RAM'] || 2560).to_i
COMPUTE_RAM = (ENV['COMPUTE_RAM'] || 2048).to_i
COMPUTE_CPUS = (ENV['COMPUTE_CPUS'] || 1).to_i
BRIDGE_INTERFACE = ENV['BRIDGE_INTERFACE'] || nil

$script = <<SCRIPT
echo "auto eth1" > /etc/network/interfaces.d/eth1.cfg
echo "iface eth1 inet dhcp" >> /etc/network/interfaces.d/eth1.cfg
ifup eth1
SCRIPT

Vagrant.configure(2) do |config|

  config.ssh.forward_agent = true

  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true

  config.vm.define 'router' do |router|
    router.vm.box = 'ubuntu/trusty64'

    router.vm.network :forwarded_port, guest: 22, host: 2221, id: 'ssh'
    router.vm.network :private_network,
      auto_config: false,
      ip: '10.0.0.201',
      virtualbox__inetnet: 'osmgmt',
      libvirt__network_name: 'osmgmt',
      libvirt__dhcp_enabled: false

    %w(virtualbox libvirt).each do |provider|
      router.vm.provider provider do |c|
        c.memory = ROUTER_RAM

        if provider == 'libvirt'
          c.machine_arch = 'x86_64'
          c.storage_pool_name = "images"
        end
      end
    end
  end

  config.vm.define 'controller' do |controller|
    controller.vm.box = 'ubuntu/trusty64'

    controller.vm.network :forwarded_port, guest: 22, host: 2222, id: 'ssh'
    controller.vm.network :private_network,
      ip: '10.0.0.2',
      auto_config: false,
      virtualbox__inetnet: 'osmgmt',
      libvirt__network_name: 'osmgmt',
      libvirt__dhcp_enabled: false,
      mac: '080027371cd9'
    controller.vm.network :private_network,
      ip: '10.0.1.2',
      auto_config: false,
      virtualbox__inetnet: 'osinst',
      libvirt__network_name: 'osinst',
      libvirt__dhcp_enabled: false
    controller.vm.network :public_network,
      mode: 'passthrough',
      auto_config: false,
      dev: BRIDGE_INTERFACE,
      bridge: BRIDGE_INTERFACE

    %w(virtualbox libvirt).each do |provider|
      controller.vm.provider provider do |c|
        c.memory = CONTROLLER_RAM

        if provider == 'libvirt'
          c.machine_arch = 'x86_64'
          c.storage_pool_name = "images"
        end

        if provider == 'virtualbox'
          c.customize ['modifyvm', :id, '--nicpromisc4', 'allow-all']
        end
      end
    end

    controller.vm.provision "shell", inline: $script
  end

  config.vm.define 'compute1' do |compute1|
    compute1.vm.box = 'ubuntu/trusty64'

    compute1.vm.network :forwarded_port, guest: 22, host: 2223, id: 'ssh'
    compute1.vm.network :private_network,
      ip: '10.0.0.3',
      auto_config: false,
      virtualbox__inetnet: 'osmgmt',
      libvirt__network_name: 'osmgmt',
      libvirt__dhcp_enabled: false,
      mac: '08002716aaa9'
    compute1.vm.network :private_network,
      ip: '10.0.1.3',
      auto_config: false,
      virtualbox__inetnet: 'osinst',
      libvirt__network_name: 'osinst',
      libvirt__dhcp_enabled: false

    %w(virtualbox libvirt).each do |provider|
      compute1.vm.provider provider do |c|
        c.memory = COMPUTE_RAM
        c.cpus = COMPUTE_CPUS

        if provider == 'libvirt'
          c.machine_arch = 'x86_64'
          c.storage_pool_name = "images"
        end
      end
    end

    compute1.vm.provision "shell", inline: $script
  end
end
