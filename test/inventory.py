#!/usr/bin/env python

import os, imp

#
# Define our testing inventory
#
TEST_INVENTORY = {
	"hosts": {
		"router": {
			"vars": {
				"network_interfaces": {
					"external": {
						"device": "eth0",
						"boot": "dhcp",
						"ipv4_address": "10.0.2.15"
					},
					"management": {
						"device": "eth1",
						"boot": "static",
						"ipv4_address": "10.0.0.201",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"controller": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "eth1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.2",
						"mac_address": "08:00:27:37:1c:d9"
					},
					"instance": {
						"device": "eth2",
						"boot": "static",
						"ipv4_address": "10.0.1.2",
						"netmask": "255.255.255.0"
					},
					"external": {
						"device": "eth3",
						"boot": "manual"
					}
				}
			}
		},
		"compute1": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "eth1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.3",
						"mac_address": "08:00:27:16:aa:a9"
					},
					"instance": {
						"device": "eth2",
						"boot": "static",
						"ipv4_address": "10.0.1.3",
						"netmask": "255.255.255.0"
					}
				}
			}
		}
	},
	"groups": {
		"router": {
			"hosts": ["router"]
		},
		"controller": {
			"hosts": ["controller"]
		},
		"compute_nodes": {
			"hosts": ["compute1"]
		},
		"ntp_servers": {
			"hosts": ["router"]
		}
	}
}

#
# Load the base inventory script, and pass it our inventory data
#
base = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'inventory.py'))
inventory = imp.load_source('inventory', base)

inventory.InventoryBuilder(TEST_INVENTORY)
