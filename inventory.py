#!/usr/bin/env python

import os
import sys
import argparse

try:
	import json
except ImportError:
	import simplejson as json

DEFAULT_INVENTORY = {
	"hosts": {
		"bc01": {
			"vars": {
				"network_interfaces": {
					"external": {
						"device": "em1",
						"boot": "static",
						"ipv4_address": "10.15.199.201",
						"gateway": "10.15.198.1",
						"netmask": "255.255.254.0",
						"dns_nameservers": "10.0.0.1",
						"dns_search": "sovee.os"
					},
					"mgmt-base": {
						"device": "em2",
						"boot": "manual",
						"commands": [
							"up link set $IFACE up promisc on",
							"down ip link set $IFACE down promisc off"
						]
					},
					"management": {
						"device": "brmgmt",
						"boot": "static",
						"ipv4_address": "10.0.0.1",
						"netmask": "255.255.255.0",
						"bridge_ports": "em2"
					},
					"inst-base": {
						"device": "em3",
						"boot": "manual",
						"commands": [
							"up link set $IFACE up promisc on",
							"down ip link set $IFACE down promisc off"
						]
					},
					"instance": {
						"device": "brinst",
						"boot": "static",
						"ipv4_address": "10.0.1.1",
						"netmask": "255.255.255.0",
						"bridge_ports": "em3"
					}
				}
			}
		},
		"bc02": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.2",
						"mac_address": "00:17:a4:77:74:08"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.2",
						"netmask": "255.255.255.0"
					},
					"external": {
						"device": "em3",
						"boot": "manual"
					}
				}
			}
		},
		"bc03": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.3",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:10"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.3",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc04": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.4",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:16"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.4",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc05": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.5",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:1C"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.5",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc06": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.6",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:22"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.6",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc07": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.7",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:2A"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.7",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc08": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.8",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:30"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.8",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc09": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.9",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:36"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.9",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc10": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.10",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:3C"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.10",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc11": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.11",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:42"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.11",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc12": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.12",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:48"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.12",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc13": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.13",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:4E"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.13",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc14": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.14",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:54"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.14",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc15": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.15",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:5A"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.15",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"bc16": {
			"vars": {
				"network_interfaces": {
					"management": {
						"device": "em1",
						"boot": "dhcp",
						"ipv4_address": "10.0.0.16",
						"netmask": "255.255.255.0",
						"mac_address": "00:17:A4:77:74:60"
					},
					"instance": {
						"device": "em2",
						"boot": "static",
						"ipv4_address": "10.0.1.16",
						"netmask": "255.255.255.0"
					}
				}
			}
		},
		"fircha": {
			"vars": {
				"network_interfaces": {
					"external": {
						"device": "p1p1",
						"boot": "static",
						"ipv4_address": "10.15.199.210",
						"gateway": "10.15.198.1",
						"netmask": "255.255.254.0",
						"dns_nameservers": "10.15.199.201",
						"dns_search": "sovee.os"
					},
					"management": {
						"device": "tap0",
						"boot": "openvpn",
						"ipv4_address": "10.0.0.100"
					},
					"instance": {
						"device": "tap1",
						"boot": "openvpn",
						"ipv4_address": "10.0.1.100"
					}
				},
				"openstack_cinder_volume_members": ["/dev/sdb", "/dev/sdc"],
				"openstack_cinder_volume_targets": ["/dev/sda", "/dev/sdb", "/dev/sdc"]
			}
		}
	},
	"groups": {
		"router": {
			"hosts": ["bc01"]
		},
		"controller": {
			"hosts": ["bc02"]
		},
		"compute_nodes": {
			"hosts": ["bc03", "bc04", "bc05", "bc06", "bc07", "bc08", "bc09", "bc10", "bc11", "bc12", "bc13", "bc14", "bc15", "bc16", "fircha"]
		},
		"ntp_servers": {
			"hosts": ["bc01"]
		}
	}
}

class InventoryBuilder(object):

	def __init__(self, inventory_data):
		self.inventory = {}
		self.read_cli_args()

		if self.args.list:
			self.inventory = self.build_inventory(inventory_data)
		elif self.args.host:
			self.inventory = self.empty_inventory()
		else:
			self.inventory = self.empty_inventory()

		print json.dumps(self.inventory)

	def read_cli_args(self):
		parser = argparse.ArgumentParser()
		parser.add_argument('--list', action = 'store_true')
		parser.add_argument('--host', action = 'store')
		self.args = parser.parse_args()

	def build_inventory(self, inventory_data):
		hosts = inventory_data['hosts']
		groups = inventory_data['groups']
		inventory = {}
		for name, group in groups.iteritems():
			inventory[name] = group

		inventory['_meta'] = { 'hostvars': {} }

		for hostname, host in hosts.iteritems():
			inventory['_meta']['hostvars'][hostname] = host['vars']

		return inventory

	def empty_inventory(self):
		return { '_meta': { 'hostvars': {} } }

if __name__ == '__main__':
	InventoryBuilder(DEFAULT_INVENTORY)
