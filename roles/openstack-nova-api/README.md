openstack-nova-api
==================

Installs and configures the Nova api service.

Role Variables
--------------

* ```openstack_nova_username```: Service username for nova.
* ```openstack_nova_password```: Service password for nova.
* ```openstack_nova_service_name```: OpenStack service name.
* ```openstack_nova_service_description```: Service API description.
* ```openstack_nova_hostname```: Hostname to register as the nova endpoint.
* ```openstack_glance_hostname```: Hostname of the Glance service host.
* ```openstack_neutron_hostname```: Hostname of the Neutron service host.
* ```openstack_neutron_username```: Username to login to the Neutron service.
* ```openstack_neutron_password```: Password to login to the Neutron service.
* ```openstack_keystone_hostname```: Hostname of the Keystone service host.
* ```openstack_metadata_proxy_shared_secret```: Shared secret used to provide metadata to booting instances.
