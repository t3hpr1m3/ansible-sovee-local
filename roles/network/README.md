network
=======

Configures network interfaces, including static and dynamic addressing.

Role Variables
--------------

* ```network_interfaces```: List if interfaces to be configured.

The format is the following:
```
network_interfaces:
  - device: eth0
    boot: static
    address: 10.0.0.1
    netmask: 255.255.255.0
    gateway: 10.0.0.1
```
