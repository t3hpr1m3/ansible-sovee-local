openstack-neutron-controller
============================

Installs the required neutron components on the controller.

Role Variables
--------------

* ```openstack_neutron_database_url```: URL pointing to the SQL database.
* ```openstack_neutron_username```: Service username for neutron.
* ```openstack_neutron_password```: Service password for neutron.
* ```openstack_neutron_service_name```: OpenStack service name.
* ```openstack_neutron_service_description```: Service API description.
* ```openstack_neutron_hostname```: Hostname to register as the neutron endpoint.
* ```openstack_neutron_local_ip```: IP address of the instance tunnel interface.
* ```openstack_neutron_ext_interface```: Network interface connected to the
external network.
* ```openstack_nova_hostname```: Host running the Nova service.
* ```openstack_nova_username```: Service username for Nova.
* ```openstack_nova_password```: Service password for Nova.
* ```openstack_keystone_hostname```: Hostname of the Keystone service host.
* ```openstack_keystone_admin_token```: Admin token for communicating with
Keystone.
* ```openstack_metadata_proxy_shared_secret```: Shared secret used to provide
metadata to booting instances.
* ```rabbitmq_hostname```: Host to use for rabbitmq access.
* ```rabbitmq_username```: User to use for rabbitmq access.
* ```rabbitmq_password```: Password to use for rabbitmq access.
