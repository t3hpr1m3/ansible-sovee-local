openstack-horizon
=================

Installs the OpenStack dashboard (horizon).

Role Variables
--------------

* ```openstack_keystone_hostname```: Hostname or the Keystone service host.
