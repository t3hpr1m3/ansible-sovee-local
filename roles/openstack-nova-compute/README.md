openstack-nova-compute
======================

OpenStack compute service (running on the compute node).

Role Variables
--------------

* ```openstack_nova_username```: Service username for nova.
* ```openstack_nova_password```: Service password for nova.
* ```openstack_nova_management_ip```: IP address of the management interface.
* ```openstack_nova_novncproxy_host```: Host running the nova-novncproxy service.
* ```openstack_nova_novncproxy_port```: Port number the nova-novncproxy service listens on.
* ```openstack_keystone_hostname```: Hostname of the Keystone service host.
* ```openstack_glance_hostname```: Hostname of the Glance service host.
* ```openstack_neutron_hostname```: Hostname of the Neutron service host.
* ```rabbitmq_hostname```: Host to use for rabbitmq access.
* ```rabbitmq_username```: User to use for rabbitmq access.
* ```rabbitmq_password```: Password to use for rabbitmq access.
