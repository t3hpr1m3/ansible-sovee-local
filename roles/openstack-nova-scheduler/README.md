openstack-nova-scheduler
========================

Controls instantiating instance on the various compute nodes.

Role Variables
--------------

* ```openstack_nova_management_ip```: IP address of the management interface.
* ```rabbitmq_hostname```: Host to use for rabbitmq access.
* ```rabbitmq_username```: User to use for rabbitmq access.
* ```rabbitmq_password```: Password to use for rabbitmq access.
