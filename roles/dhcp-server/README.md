dhcp-server
===========

Configures the dhcp server for providing IP's to the nodes in the cluster.  Can
also configure pxe booting to allow for automated installs.

This role is a dependency of the pxe-server role, so there is no need to include
it directly.  Static IP's will be assigned to all hosts included in the
`subnet_groups` list.  Without this, there would be no way to assiciate a given
inventory host with its corresponding physical host.

Role Variables
--------------

* ```subnet_address```: Base address for the subnet (ex. 10.0.0.0).
* ```subnet_netmask```: Netmask for this subnet.
* ```subnet_dynamic_start```: Beginning range for dynamic IP's.
* ```subnet_dynamic_end```: End range for dynamic IP's.
* ```subnet_broadcast_address```: Broadcast network address to use for this
subnet.
* ```subnet_domain_name```: Domain name assigned to this subnet for DNS lookups.
* ```subnet_groups```: Specifies which inventory groups should receive fixed
addresses.
* ```subnet_routers```: Router IP's to advertise on this subnet.
* ```subnet_nameservers```: DNS server IP's to advertise on this subnet.
* ```listening_interfaces```: Host interfaces to listen on.
* ```pxe_enabled```: Should pxe boot be configured.
* ```pxe_filename```: Default filename to serve for pxe booting.
* ```pxe_tftp_server```: IP address of the tftp server.
