openstack-nova-conductor
========================

Mediates database access between the nova-compute service and the SQL db.

Role Variables
--------------

* ```openstack_nova_database_url```: URL pointing to the SQL database.
* ```openstack_nova_management_ip```: IP address of the management interface.
* ```rabbitmq_hostname```: Host to use for rabbitmq access.
* ```rabbitmq_username```: User to use for rabbitmq access.
* ```rabbitmq_password```: Password to use for rabbitmq access.
