router
======

Configures the host to act as a router for the other nodes in the cluster.
Currently, that merely involves allowing forwarding of packets between internal
nodes and the outside world.
