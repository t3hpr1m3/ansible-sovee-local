openstack-glance
================

Installs and configures the Glance image service.  This service is responsible
for maintaining VM images used to spin up new instances.

Role Variables
--------------

* ```openstack_glance_database_url```: URL pointing to the SQL database.
* ```openstack_glance_username```: Service username for glance.
* ```openstack_glance_password```: Service password for glance.
* ```openstack_glance_service_name```: OpenStack service name.
* ```openstack_glance_service_description```: Service API description.
* ```openstack_glance_hostname```: Hostname to register as the glance endpoint.
* ```openstack_glance_image_path```: Directory used to store images.
* ```openstack_keystone_hostname```: Hostname of the keystone service host.
