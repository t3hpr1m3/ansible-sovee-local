openstack-nova-consoleauth
==========================

Authenticates VNC connections to running instances.


Role Variables
--------------

* ```openstack_nova_management_ip```: IP address of the management interface.
