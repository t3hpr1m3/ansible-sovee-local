openstack-keystone
==================

Installs and configures the Keystone authentication service.

Role Variables
--------------

* ```openstack_keystone_database_url```: URL pointing to the SQL database.
* ```openstack_keystone_service_name```: OpenStack service name
* ```openstack_keystone_service_description```: Service API description
* ```openstack_keystone_hostname```: Hostname to register as the keystone endpoint
* ```openstack_keystone_memcache_servers```: (optional) memcache servers to use in Keystone
