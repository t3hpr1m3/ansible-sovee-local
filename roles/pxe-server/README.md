pxe-server
=========

Installs and configures tftpd to handle automatic provisioning of bare-metal
nodes.  Includes dhcp-server for IP assignment and apache2 for serving install
media.

Role Variables
--------------

* ```management_ip```: IP address the tftp server should listen on.
* ```sovee_ssh_key```: SSH key to inject into the sovee user's account when the
node is initially provisioned.
