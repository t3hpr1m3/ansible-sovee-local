openstack-cinder-storage
========================

Installs the storage portions of the Cinder block service.  This service
provides persistent disks to instances.

Role Variables
--------------

* ```openstack_cinder_database_url```: URL pointing to the SQL database.
* ```openstack_cinder_hostname```: Host running the Cinder volume services.
* ```openstack_cinder_username```: Service username for Cinder.
* ```openstack_cinder_password```: Service password for Cinder.
* ```openstack_cinder_management_ip```: IP address on the management network.
* ```openstack_glance_hostname```: Hostname of the Glance service host.
* ```openstack_keystone_hostname```: Hostname of the Keystone service host.
* ```rabbitmq_hostname```: Host to use for rabbitmq access.
* ```rabbitmq_username```: User to use for rabbitmq access.
* ```rabbitmq_password```: Password to use for rabbitmq access.
