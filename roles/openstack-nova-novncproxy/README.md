openstack-nova-novncproxy
=========================

Allows connecting to running instances via VNC.

Role Variables
--------------

* ```openstack_nova_management_ip```: IP address of the management interface.
