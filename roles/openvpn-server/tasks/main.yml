---
- name: ensure openvpn traffic is allowed
  template:
    src: etc/ferm/ferm.d/02-openvpn.conf.j2
    dest: /etc/ferm/ferm.d/02-openvpn.conf
    owner: root
    group: root
    mode: 0644
  notify: restart ferm

- name: ensure required openvpn packages are installed
  apt:
    name: "{{ item }}"
    state: present
  with_items:
    - openvpn
    - easy-rsa
    - bridge-utils

- name: ensure required directories are present
  file:
    name: "{{ item }}"
    state: directory
  with_items:
    - /etc/openvpn
    - /etc/openvpn/easy-rsa
    - /etc/openvpn/easy-rsa/keys
    - /etc/openvpn/ccd/mgmt
    - /etc/openvpn/ccd/inst

- stat: path=/etc/openvpn/easy-rsa/build-key
  register: easy_rsa_build_key

- name: ensure the easy-rsa files exist
  synchronize:
    src: /usr/share/easy-rsa/
    dest: /etc/openvpn/easy-rsa/
  delegate_to: "{{ inventory_hostname }}"
  when: easy_rsa_build_key.stat.exists == False

- name: ensure the certificates are present
  copy:
    src: etc/openvpn/{{ item }}
    dest: /etc/openvpn/{{ item }}
    owner: root
    group: root
    mode: 0644
  with_items:
    - ca.crt
    - sovee.crt

- name: ensure the key file is present
  copy:
    src: etc/openvpn/sovee.key
    dest: /etc/openvpn/sovee.key
    owner: root
    group: root
    mode: 0600

- name: ensure the Diffie-Hellman key is present
  command: openssl dhparam -out /etc/openvpn/dh2048.pem 2048 creates=/etc/openvpn/dh2048.pem

- name: ensure the external vpn is configured
  template:
    src: etc/openvpn/external.conf.j2
    dest: /etc/openvpn/external.conf
    owner: root
    group: root
    mode: 0644
  notify:
    - restart openvpn

- name: ensure the fircha scripts are present
  template:
    src: "etc/openvpn/{{ item }}.j2"
    dest: "/etc/openvpn/{{item }}"
    owner: root
    group: root
    mode: 0700
  with_items:
    - fircha_up.sh
    - fircha_down.sh
  notify:
    - restart openvpn

- name: ensure the management vpn is configured
  template:
    src: etc/openvpn/fircha-mgmt.conf.j2
    dest: /etc/openvpn/fircha-mgmt.conf
    owner: root
    group: root
    mode: 0644
  notify:
    - restart openvpn

- name: ensure the instance vpn is configured
  template:
    src: etc/openvpn/fircha-inst.conf.j2
    dest: /etc/openvpn/fircha-inst.conf
    owner: root
    group: root
    mode: 0644
  notify:
    - restart openvpn

- name: ensure static ip configurations for fircha exist
  template:
    src: "etc/openvpn/ccd/{{ item }}/fircha.j2"
    dest: "/etc/openvpn/ccd/{{ item }}/fircha"
    owner: root
    group: root
    mode: 0644
  with_items:
    - mgmt
    - inst
  notify:
    - restart openvpn

- name: ensure openvpn is running
  service:
    name: openvpn
    state: started
