apache2
=======

Installs and configures the apache2 webserver.  By default, it will open port 80
to incoming traffic.

Role Variables
--------------

* ```allow_http_traffic```: Determines whether port 80 should be allowed through
the firewall.
* ```apache2_interface```: Interface to allow traffic to.
