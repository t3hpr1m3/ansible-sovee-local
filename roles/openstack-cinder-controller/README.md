openstack-cinder-controller
===========================

Installs the controller portions of the Cinder block service.  This service
provides persistent disks to instances.

Role Variables
--------------

* ```openstack_cinder_database_url```: URL pointing to the SQL database.
* ```openstack_cinder_hostname```: Host running the Cinder controller services.
* ```openstack_cinder_username```: Service username for Cinder.
* ```openstack_cinder_password```: Service password for Cinder.
* ```openstack_cinder_service_name```: OpenStack name for the Cinder service.
* ```openstack_cinderv2_service_name```: OpenStack name for the CinderV2
service.
* ```openstack_cinder_service_description```: Service API description.
* ```openstack_cinder_management_ip```: IP address on the management network.
* ```openstack_keystone_hostname```: Hostname of the keystone service host.
* ```openstack_keystone_admin_token```: Token to use to communicate with
Keystone.
* ```rabbitmq_hostname```: Host to use for rabbitmq access.
* ```rabbitmq_username```: User to use for rabbitmq access.
* ```rabbitmq_password```: Password to use for rabbitmq access.
