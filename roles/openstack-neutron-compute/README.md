openstack-neutron-compute
=========================

Installs the required Neutron components on the compute node.

Role Variables
--------------

* ```openstack_neutron_username```: Service username for Neutron.
* ```openstack_neutron_password```: Service password for Neutron.
* ```openstack_neutron_local_ip```: IP address of the instance tunnel interface.
* ```openstack_keystone_hostname```: Hostname of the Keystone service host.
* ```rabbitmq_hostname```: Host to use for rabbitmq access.
* ```rabbitmq_username```: User to use for rabbitmq access.
* ```rabbitmq_password```: Password to use for rabbitmq access.
