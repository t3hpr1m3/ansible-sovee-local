ferm
=========

Configures the firewall using ferm.

Role Variables
--------------

* ```ferm_enabled```: Whether or not to enable ferm
* ```ferm_default_policy_input```: Default policy for INPUT chain (default: ```DROP```)
* ```ferm_default_policy_output```: Default policy for OUTPUT chain (default: ```ACCEPT```)
* ```ferm_default_policy_forward```: Default policy for FORWARD chain (default: ```DROP```)
